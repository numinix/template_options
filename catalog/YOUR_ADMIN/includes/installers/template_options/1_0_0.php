<?php

// Create Template Configuration Group

$db->Execute("CREATE TABLE IF NOT EXISTS " . TABLE_TEMPLATE_CONFIG_GROUP . " (
	`template_configuration_group_id` int(11) NOT NULL AUTO_INCREMENT,
	`template_configuration_group_title` varchar(64) NOT NULL,
	`template_group_description` varchar(255) NOT NULL,
	`sort_order` int(5) NOT NULL,
	`visible` int(1) NOT NULL,
	PRIMARY KEY (`template_configuration_group_id`)
);");


// Create Template Configuration

$db->Execute("CREATE TABLE IF NOT EXISTS " . TABLE_TEMPLATE_CONFIG . " (
	`template_configuration_id` int(11) NOT NULL AUTO_INCREMENT,
	`template_configuration_title` text NOT NULL,
	`template_configuration_tab` text NOT NULL,
	`template_configuration_tab_section` text NOT NULL,
	`template_configuration_key` varchar(255) NOT NULL,
	`template_configuration_value` text NOT NULL,
	`template_configuration_description` text NOT NULL,
	`template_configuration_group_id` int(11) NOT NULL,
	`sort_order` int(5) DEFAULT NULL,
	`set_function` text DEFAULT NULL,
	PRIMARY KEY (`template_configuration_id`)
);");

$configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Template Options' ORDER BY configuration_group_id ASC;");
if ($configuration->RecordCount() > 0) {
  while (!$configuration->EOF) {
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $configuration->MoveNext();
  }
}

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('Template Options', 'Set Template Options', '1', '1');");
$configuration_group_id = $db->Insert_ID();

$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Version', 'TO_VERSION', '1.0.0', 'Version installed:', " . $configuration_group_id . ", 0, NOW(), NOW(), NULL, NULL);"); 